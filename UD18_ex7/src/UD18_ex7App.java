import java.sql.Connection;

import dto.DatabaseDto;
import dto.createTableDto;
import methods.closeConnection;
import methods.createDB;
import methods.createTable;
import methods.insertData;
import methods.openConnection;

public class UD18_ex7App {
	
	public static Connection conexion;

	public static void main(String[] args) {
	
        DatabaseDto newDatabase = new DatabaseDto("ejercicio7");
        
        createTableDto PROYECTO = new createTableDto(newDatabase, "PROYECTO");
        createTableDto CIENTIFICOS = new createTableDto(newDatabase, "CIENTIFICOS");
        createTableDto ASIGNADO_A = new createTableDto(newDatabase, "ASIGNADO_A");
        
        String CreacionTabla1 = "CREATE TABLE `ejercicio7`.`PROYECTO` (`Id` CHAR(4) NOT NULL,`Nombre` VARCHAR(255) NOT NULL,`Horas` INT NULL,PRIMARY KEY (`Id`));";
        String CreacionTabla2 = "CREATE TABLE `ejercicio7`.`CIENTIFICOS` (`DNI` VARCHAR(8) NOT NULL,`NomApels` VARCHAR(255) NULL,PRIMARY KEY (`DNI`));";
        String CreacionTabla3 = "CREATE TABLE `ejercicio7`.`ASIGNADO_A` (`Cientifico` VARCHAR(8) NOT NULL,`Proyecto` CHAR(4) NOT NULL,PRIMARY KEY (`Cientifico`, `Proyecto`),INDEX `Proyecto_idx` (`Proyecto` ASC) VISIBLE,CONSTRAINT `Cientifico`FOREIGN KEY (`Cientifico`)REFERENCES `ejemplo1`.`CIENTIFICOS` (`DNI`)ON DELETE NO ACTION ON UPDATE NO ACTION,CONSTRAINT `Proyecto`FOREIGN KEY (`Proyecto`)REFERENCES `ejemplo1`.`PROYECTO` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION);";
        
        openConnection.openConnection();
        createDB.createDB(newDatabase);
        createTable.createTable(newDatabase, PROYECTO, CreacionTabla1);
        createTable.createTable(newDatabase, CIENTIFICOS, CreacionTabla2);
        createTable.createTable(newDatabase, ASIGNADO_A, CreacionTabla3);
        insertData.insertData(newDatabase, "PROYECTO", "(Id, Nombre, Horas) VALUE('k', Nombre, 80);");
        closeConnection.closeConnection();
	}

}
