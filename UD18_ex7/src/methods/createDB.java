package methods;

import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

import dto.DatabaseDto;



public class createDB {

public static void createDB(DatabaseDto name) {
	 try {
         String query = "CREATE DATABASE " + name.getNombre();
         Statement st = openConnection.conexion.createStatement();
         st.executeUpdate(query);
     }catch(SQLException e) {
         System.out.println("Error creating the new database");
     }
}

}
