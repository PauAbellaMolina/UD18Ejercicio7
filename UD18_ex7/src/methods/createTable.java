package methods;

import java.sql.SQLException;
import java.sql.Statement;

import dto.DatabaseDto;
import dto.createTableDto;

public class createTable {
	
	public static void createTable(DatabaseDto name, createTableDto nameTabla, String CreacionTabla) {
		try {
			String Querydb = "USE " + name.getNombre() +";";
			Statement stdb = openConnection.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = CreacionTabla;
			Statement st = openConnection.conexion.createStatement();
			st.executeLargeUpdate(Query);
			System.out.println("Tabla creada con exito");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla");
		}
		
	}

}
